<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'max_attempt' => $this->max_attempt,
            'threshold' => $this->threshold,
            'award' => $this->award,
            'count_questions' => $this->count_questions
        ];
    }
}
