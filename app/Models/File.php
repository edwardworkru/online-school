<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['original_name', 'extension', 'path'];

    protected $guarded = ['id'];

    public function related()
    {
        return $this->morphTo();
    }
}
