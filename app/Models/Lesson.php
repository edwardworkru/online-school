<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable = ['title', 'course_id', 'account_id'];

    protected $guarded = ['id'];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function tests()
    {
        return $this->hasMany(Test::class);
    }

    public function video()
    {
        return $this->hasOne(Video::class);
    }

    public function file()
    {
        return $this->morphOne(File::class, 'related');
    }
}
