<?php

namespace App\Models;

use App\Scopes\YouTubeScope;
use Illuminate\Database\Eloquent\Model;

class YouTubeVideo extends Video
{
    protected $table = 'videos';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new YouTubeScope);
    }
}
