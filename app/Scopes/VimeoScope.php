<?php
/**
 * Created by PhpStorm.
 * User: Edward
 * Date: 09.03.2019
 * Time: 0:19
 */

namespace App\Scopes;


class VimeoScope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param Builder $builder
     * @param Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('host', 'vimeo');
    }
}