<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Account::class, function (Faker $faker) {
    return [
        'title' => Str::random(10)
    ];
});
