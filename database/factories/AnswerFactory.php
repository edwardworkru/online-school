<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Answer::class, function (Faker $faker) {
    return [
        'title' => Str::random(5),
        'is_true' => rand(0, 1) > 0,
    ];
});
