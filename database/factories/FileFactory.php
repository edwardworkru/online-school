<?php

use Faker\Generator as Faker;

$factory->define(App\Models\File::class, function (Faker $faker) {
    // TODO: Rewrite factory for file
    return [
        'original_name' => 'dump.json',
        'extension' => 'json',
        'path' => 'localhost:3000/dump.json'
    ];
});
