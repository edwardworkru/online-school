<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Lesson::class, function (Faker $faker) {
    return [
        'title' => Str::random(10),
        'account_id' => rand(1, 2)
    ];
});
