<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Question::class, function (Faker $faker) {
    return [
        'title' => Str::random(7),
        'type' => rand(1, 2),
    ];
});
