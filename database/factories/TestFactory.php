<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Test::class, function (Faker $faker) {
    return [
        'max_attempt' => rand(3, 5),
        'threshold' => rand(60, 90),
        'award' => rand(5, 20)
    ];
});
