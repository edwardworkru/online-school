<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Models\Video::class, function (Faker $faker) {

    $temp = Str::random(7);

    return [
        'host' => 'youtube',
        'source_id' => $temp,
        'original_path' => 'https://www.youtube.com/watch?v='.$temp
    ];
});
