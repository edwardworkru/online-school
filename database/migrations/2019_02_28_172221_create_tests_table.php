<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('lesson_id')->comment('Relation with lesson');
            $table->integer('max_attempt')->default(3)->comment('Max attempt to success');
            $table->integer('threshold')->default(70)->comment('Threshold to success in %');
            $table->integer('award')->default(10)->comment('Award per success test');
            $table->integer('count_questions')->default(5)->comment('Count question for current test');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
