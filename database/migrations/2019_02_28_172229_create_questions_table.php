<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    const ONE_ANSWER = 1; // This mean that question have just one correct answer
    const MANY_ANSWER = 2; // This mean that question have more than one correct answer

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('type')->default(self::ONE_ANSWER)->comment('Type of question');
            $table->string('title')->comment('Title of question');
            $table->integer('test_id')->comment('Relation with test');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
