<?php

use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Account::create(
            [
                'title' => 'Basic'
            ]
        );

        App\Models\Account::create(
            [
                'title' => 'Premium',
                'price' => 80
            ]
        );
    }
}
