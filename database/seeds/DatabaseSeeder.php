<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Start seeds');

        $this->call(UsersTableSeeder::class);
        $this->call(AccountsTableSeeder::class);

        // Create a lot of courses and related data
        factory(App\Models\Course::class, 5)
            ->create()
            // For each created course create related lessons
            ->each(function ($course) {
                $course->lessons()
                    ->saveMany(factory(App\Models\Lesson::class, 5)
                    ->create(['course_id' => $course->id])
                    // For each created lesson create related test
                    ->each(function ($lesson) {

                        // For each lesson create video
                        $lesson->video()
                            ->save(factory(App\Models\Video::class)
                            ->create(['lesson_id' => $lesson->id]));

                        // For each lesson create dummy file
                        $lesson->file()
                            ->save(factory(App\Models\File::class)
                            ->create([
                                'related_id' => $lesson->id,
                                'related_type' => 'App\Models\Lesson',
                            ]));

                        $lesson->tests()
                            ->saveMany(factory(App\Models\Test::class, 1)
                            ->create(['lesson_id' => $lesson->id])
                            // For each created test create related questions
                            ->each(function ($test) {
                                $test->questions()
                                    ->saveMany(factory(App\Models\Question::class, 5)
                                    ->create(['test_id' => $test->id])
                                    // For each created question create related answers
                                    ->each(function ($question) {
                                        $question->answers()
                                            ->saveMany(factory(App\Models\Answer::class, 5)
                                            ->create(['question_id' => $question->id])
                                        );
                                    })
                                );
                            })
                        );

                    })
                );
            });
    }
}
