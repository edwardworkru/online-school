<?php

use Illuminate\Database\Seeder;

class TestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lessons = App\Models\Lesson::all();

        $lessons->each(function ($lesson) {
            $lesson->tests()->save(factory(App\Models\Test::class)->make());
        });
    }
}
