<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('/courses', 'CourseController');

Route::apiResource('/lessons', 'LessonController');

Route::apiResource('/files', 'FileController');

Route::apiResource('/videos', 'VideoController');

Route::apiResource('/tests', 'TestController');

Route::apiResource('/questions', 'QuestionController');

Route::apiResource('/questions/{question}/answers', 'AnswerController');

Route::apiResource('/accounts', 'AccountController');
